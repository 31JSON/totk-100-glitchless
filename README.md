# TLoZ Tears of the Kingdom 100% Glitchless

This is a route document for a TotK 100% Glitchless play through or speedrun.

## Route Information

Overall organization of the route is as follows:
- Great Sky Island 1
- Crisis at Hyrule Castle 1 (Paraglider)
- Great Plateau 1
- Autobuild
- Central Hyrule Depths
- Wind Temple
- Great Plateau 2
- Kakariko 1
- Naydra farming
- Hateno 1
- Hebra-Tabantha Depths
- Lightcast Island
- Starview Island
- Hyrule Castle 1
- Fire Temple
- Dinraal farming
- Tarrey Town
- Akkala Depths
- Eldin Depths
- Woodland Depths
- Water Temple
- Zonaite Forge Island
- Great Sky Island 2
- Farosh farming
- Lightning Temple
- Wasteland Depths
- Gerudo Highlands Depths
- Lake Depths
- Faron Depths
- East Necluda Depths
- Crisis at Hyrule Castle 2 (Defeat Fake Zelda)
- Thunderhead and Dragonhead Islands
- Spirit Temple
- Korok Forest
- All Memories
- Master Sword
- White Dragon farming
- Ganondorf 1
- Lanayru Depths
- West Necluda Depths
- East Woodlands
- South Tabantha
- East Lanayru
- Floating Coliseum
- Ridgeland Depths
- Zora's Domain Depths
- South Lake
- Gerudo Canyon
- Ridgeland and Satori Mountain
- North Eldin
- Eldin clean up
- Zora's Domain clean up
- Hebra clean up
- Wasteland clean up
- Dueling Peaks clean up
- Faron clean up
- Necluda clean up
- All Shrines
- Sky clean up
- Hateno clean up
- 100% Surface clean up
- Save Hyrule

## Objectives

For a list of the objectives of the playthrough see the list linked below.

[List of Objectives to complete 100%](https://codeberg.org/31JSON/totk-100-glitchless/src/branch/main/requirements.md)

## Viewing the route

The route can be viewed using a tool called `Not-Celer`. Clicking the link below will open the route doc in `Not-Celer`.

[View the route in Not-Celer](https://restite.org/notceler-totk/?codeberg=31JSON/totk-100-glitchless/raw/totk-glitchless-hundo-notceler.txt)

When viewing the route, you can press `Ctrl+G` to view help on how to use it.

## Commentary

The complete video posted on BeardBear's channel has no commentary. The YouTube playlist below has my original Twitch VODs with commentary and more information. The play through is broken into 10 hour segments.

[Commentary Playlist](https://www.youtube.com/playlist?list=PLLoed501OihEk2yVFeUgpC088hQtho3PE)

## Thanks and Acknowledgements

- [savage](https://restite.org/) for developing `Not-Celer` along with a plethora of other tools, including the Object Map, that made it possible to create this route. Visit his website for links to his other tools. You can find him in the discord, and he very occasionally [streams on Twitch](https://www.twitch.tv/savagei3).
- [TheSquirrelSaysMeow](https://www.twitch.tv/thesquirrelsaysmeow) beta tested the route on stream.
- kn1tt3r for beta testing the route, and valuable feedback.
- [peri](https://periwinkle.sh/) for her development of [mist](https://periwinkle.sh/projects/mist/), my preferred speedrun timer, and [periscope](https://codeberg.org/periwinkle/periscope), a homebrew solution for Nintendo Switch input display. I use both of these tools when streaming. She can be found in the discord and [streaming on Twitch](https://www.twitch.tv/lieutenantperiwinkle).
- Piston for his work on [Celer](http://celer.pistonite.org/docs/), the routing tool which `Not-Celer` is based upon. You can find him in the discord and [streaming on Twitch](https://www.twitch.tv/pistonight).
- [BeardBear](http://www.youtube.com/@BeardBear) for his support of this project.

## Twitch - Discord

I stream Tears of the Kingdom and Breath of the Wild 100% run on Twitch. You can follow me there.
- https://www.twitch.tv/31json

Our Discord server is open to everyone. If you have questions or just want to hang out with fan of TLoZ series, join us.
- https://discord.gg/hylian
